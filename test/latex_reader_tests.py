import unittest
from lxml import etree
from propopub.latex_poem_reader import LatexPoemReader
from propopub.render import HTMLRenderable
from test.data import Data


def render_html_and_print(renderable: HTMLRenderable):
    print((etree.tostring(renderable.render_html(), pretty_print=True)).decode())


def parse_poem_visual_test():
    data = Data()
    tex_poem = data.LATEX_POEM
    parsed_poem = LatexPoemReader.parse_poem(tex_poem)
    render_html_and_print(parsed_poem)

if __name__ == '__main__':
    parse_poem_visual_test()
