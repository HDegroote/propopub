from propopub.poem import Line, Poem


class Data:

    LINE1 = Line("line 1")
    LINE2 = Line("line 2")
    POEM_OF_2_LINES = Poem(title="2 line poem", contents=[LINE1, LINE2])
    LATEX_POEM = r"""
\include{definitions}
\documentclass{book}
\usepackage{verse}
\begin{document}	
	\poemtitle{Test poem}
	\begin{verse}						
		I am a test poem
		I have no point and am a liar.	
	\end{verse}	
\end{document}
        """