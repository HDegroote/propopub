from lxml import etree
from propopub.render import HTMLRenderable
from test.data import Data


def render_html_and_print(renderable: HTMLRenderable):
    print((etree.tostring(renderable.render_html(), pretty_print=True)).decode())


def test_2line_poem():
    data = Data()
    render_html_and_print(data.POEM_OF_2_LINES)


if __name__ == '__main__':
    test_2line_poem()
