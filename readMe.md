# ProPoPub: Prose and Poetry Publication Service

This tool is for those who wish to self-publish their writings.
It takes your texts organised per collection, and publishes them as a simple website. 
Currently only poetry is supported, but prose will be included with the next release.

Note that this package is still under development (alfa version), 
although the basic functionality should work. 

Note that the system creates the website, but does not host it. 
The easiest approach to hosting is probably to use a peer-to-peer browser like [Beaker](https://beakerbrowser.com/),
although you can obviously also host it on a standard server.

### Requirements
Python 3.8 or later.

Dependency management with [poetry](https://python-poetry.org/) is supported 
(and recommended for obvious irrational reasons). 
However, you can also simply install the requirements as follows:

    pip install -r requirements.txt


### Input format
To use this tool, your texts should be written in [latex](https://www.latex-project.org/),
albeit in a very limited subset.
This allows the system to parse basic semantic information—like when a verse ends—and transform it into HTML.
Note that you do not need to know Latex to use this tool:
just have a look at the [examples](example/poems), which are auto-explanatory. 
 
Texts are combined in collections. 
Each collection is defined by a simple json file listing its content.
Click [here](example/Example_collection.json) for an example.



## Using the system

### Step 1: Create a directory for your poetry

### Step 2: Create a latex file for each poem

The minimal structure is something like:

	\poemtitle{My First Publication}
	\begin{verse}
		I wish I were well-versed in verse\\
		But this is my first creation\\		
		Yet for better or for worse\\
		I pursue its publication\\!
	\end{verse}

This is transformed by the system into [this](example/html/poems/My_First_Publication.html) web page.

Have a look at the other [examples](example/) to see some less trivial formatting in action. 
The latex source files used for them are to be found [here](example/poems)


### Step 3: Create a .json file with the title and content of your poetry collection
 
    {
      "title": "Example Collection",  
      "content": [
        "I Wish I Were Well-Versed in Verse"
      ]
    }
  The system will try to match the titles you specifiy here with the titles of your poems. 
  There is some margin for typos, but if no similar title is found, an error will be thrown.

### Step 4: Run the application to create the website

    python main.py poem_dir collection.json html_dir

Note: if the html_dir already exists, you must add the --existsok flag to give the system permission to replace the existing directory and its contents

        python main.py poem_dir collection.json html_dir --existsok

Note: add the --forlocaltest flag if you wish to embed the styles within each html page. 
This is useful for inspecting the rendered HTML pages locally, without hosting them.
It embeds the css files, and makes all links relative,
whereas normally the reference to the style files and poems are root-relative 
and therefore not loaded correctly when you open 
an html page from your local file system 

    python main.py poem_dir collection.json html_dir --forlocaltest

For example, to recreate the example website, run the following command from the project directory:

    python main.py example/poems example/Example_collection.json example/html --overwritehtml --forlocaltest

### Step 5: Host the website
Running Step 4 creates a directory at the location specified by argument 'htmldir'.
This directory contains all the files required 
to create the website (HTML and CSS files). All you still need to do is host it.

#### Option 1: Beaker browser
[Beaker](https://beakerbrowser.com/), is peer-to-peer browser.
Its big advantage is that it allows you to easily host your website, even without owning a server.
Its big disadvantage is that all visitors to your website also need Beaker browser, 
as the peer-to-peer protocol is currently not supported by standard browsers.

To host your website using Beaker: click "+ New Drive" and select "From Folder". Select the HTML folder rendered 
by the ProPoPub system, then click create. 
That's it! 
Share the link with friends, and they will be able to visit your website.

#### Option 2: Docker
You can use the Apache server docker image to run the server.
This is particularly useful for testing the website locally.

This command hosts your server as created in directory "HTMLDIR" on localhost:8080

    sudo docker run -d --mount type=bind,source="HTMLDIR",target=/usr/local/apache2/htdocs/,readonly --name mysite -p 8080:80 httpd

Because the HTML directory is mounted as a docker volume, 
any changes you make to the files will automatically be synced with the docker image.


### Poetry formatting support
This tool supports basic poetry formatting options such as indenting lines,
separating a poem in chapters,
adding a quotation at the start of a poem
and right-aligning text.

Lines which are too long to de displayed on a single line will continue right-aligned on the next line,
as is the standard choice in printed poetry publications.

Feel free to open an issue if you would like to see another formatting option.
I'm adding more functionality as the need arises. 




 ### Extensions
 - Support for prose is coming soon
 - The layout is currently very minimalist. 
   Do feel free to play around with the .css files.
   - If any design-minded person would like to create a prettier standard design, please do contact me.
 
