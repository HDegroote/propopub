import argparse
from pathlib import Path
from typing import Tuple
import logging_helpers
from propopub import workflow


def parse_args() -> Tuple[Path, Path, Path, bool, bool]:
    parser = argparse.ArgumentParser(description='Create a website with poetry and prose')
    parser.add_argument('latexdir',
                        type=str,
                        help='The directory with the source latex files')
    parser.add_argument('collectioninfo',
                        type=str,
                        help='A json file which describes the collection (title, subtitle?, contents')
    parser.add_argument('htmldir',
                        type=str,
                        help='The directory where the website should be created '
                             '(assumes the parent directory exists)')
    parser.add_argument('-o', '--overwritehtml',
                        help='Proceed even if the htmldir already exists (overwriting existing files with their new '
                             'version)',
                        action="store_true")

    parser.add_argument('-e', '--forlocaltest',
                        help='Allows you to inspect the HTML page locally by embedding the css style sheets amnd making'
                             ' the links relative. Useful for debugging purposes, but discouraged otherwise.',
                        action="store_true")

    args = parser.parse_args()
    latex_dir = Path(args.latexdir)
    html_dir = Path(args.htmldir)
    collection_json_loc = Path(args.collectioninfo)
    overwrite_htmldir = args.overwritehtml
    for_local_test = args.forlocaltest
    return latex_dir, html_dir, collection_json_loc, overwrite_htmldir, for_local_test


def main():
    logging_helpers.setup_logging()
    latex_dir, html_dir, collection_json_loc, overwrite_htmldir, for_local_test = parse_args()

    if html_dir.exists() and not overwrite_htmldir:
        raise ValueError("Please set the --overwritehtml option to add to the existing html directory, "
                         "or specify a directory"
                         "which does not yet exist")

    workflow.run_workflow(latex_dir=latex_dir,
                          html_dir=html_dir,
                          collection_json_loc=collection_json_loc,
                          is_for_local_test=for_local_test
                          )


if __name__ == "__main__":
    main()
