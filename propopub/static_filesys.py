import logging
from pathlib import Path

logger = logging.getLogger(__name__)


PROJECT_LOC = Path("propopub")
assert PROJECT_LOC.is_dir()
STATIC = PROJECT_LOC / "static"
POEM_CSS_LOC = STATIC / "poem_style.css"
COLLECTION_CSS_LOC = STATIC / "collection_style.css"

DATABASE_LOC = PROJECT_LOC / "database"
if not DATABASE_LOC.is_dir():
    logger.info(f"No database directory exists at {DATABASE_LOC}--creating it")
    DATABASE_LOC.mkdir()
POEMS_JSON = DATABASE_LOC / "poems.json"
COLLECTIONS_JSON = DATABASE_LOC / "collections.json"
COLLECTION_JSON_TITLE = "title"
COLLECTION_JSON_SUBTITLE = "subtitle"
COLLECTION_JSON_CONTENT = "content"
COLLECTION_JSON_HTML_LOCATION = "html_location"



