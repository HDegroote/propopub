from pathlib import Path

POEM_STYLESHEET_HTML_LOCATION = Path("poem_style.css")
COLLECTION_STYLESHEET_HTML_LOCATION = Path("collection_style.css")

POEMS_LOC = Path("poems")
# PROSE_LOC = Path("prose")
COLLECTIONS_LOC = Path("collections")
INDEX_LOC = Path("index.html")


def setup_html_filesys(html_root: Path):
    (html_root / POEMS_LOC).mkdir(exist_ok=True)
    # (html_root / PROSE_LOC).mkdir(exist_ok=True)
    (html_root / COLLECTIONS_LOC).mkdir(exist_ok=True)


def get_root_relavite_location(path: Path, html_root: Path) -> Path:
    return Path("/" + str(path.relative_to(html_root)))
