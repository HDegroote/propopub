import abc
import os
from pathlib import Path
from xml.etree.ElementTree import Element

from lxml import etree
from lxml.builder import E


class HTMLRenderable(abc.ABC):

    @abc.abstractmethod
    def render_html(self) -> Element:
        pass

    @staticmethod
    def CLASS(v):
        # helper function, 'class' is a reserved word
        return {'class': v}

    @staticmethod
    def ID(v):
        # helper function, 'id' is a reserved word
        return {'id': v}


def wrap_in_html_page(renderable: HTMLRenderable,
                      href: str,
                      title: str,
                      embedded_css: str="") -> etree.Element:
    rendered_renderable = renderable.render_html()
    body = E.body(rendered_renderable)

    head_content = [
        E.title(title),
        E.link(rel="stylesheet", href=str(href)),
        E.meta(charset="utf-8")
    ]

    if len(embedded_css) > 0:
        style = E.style(type="text/css")
        style.text = embedded_css
        head_content.append(style)

    head = E.head(*head_content)
    html_as_etree = E.html(head, body)
    return html_as_etree


def convert_to_relative_html_path(path_to_link_to: Path, path_to_link_from: Path) -> str:
    res = os.path.relpath(path_to_link_to, path_to_link_from)
    return str("/" + res)
