import json
import logging
import shutil
from pathlib import Path
from typing import Iterable
from lxml import etree
from regex import regex
from propopub import render, static_filesys, db_interface, html_filesys, html_norm_helpers
from propopub.collection import Collection
from propopub.latex_poem_reader import LatexPoemReader
from lxml.builder import E

from propopub.render import HTMLRenderable
logger = logging.getLogger(__name__)

def create_html_page_for_poem(poem: etree.Element) -> etree.Element:
    body = E.body(poem)
    head = E.head(
        E.link(rel="stylesheet", href="style.css"),
        E.meta(charset="utf-8")
    )
    html = E.html(head, body)
    return html


def parse_poems(latex_files: Iterable[Path], html_root_dir: Path, embed_style=False):
    for latex_file in latex_files:
        parse_latex_file(latex_file,
                         html_root_dir=html_root_dir,
                         embed_style=embed_style)


def parse_latex_file(latex_file: Path, html_root_dir: Path, embed_style=False) -> Path:
    logger.info(f"Parsing file {latex_file}")
    with open(latex_file, "r") as file:
        latex_content = file.read()
    parsed_poem = LatexPoemReader.parse_poem(latex_content)

    css_target_loc = "/" + str(html_filesys.POEM_STYLESHEET_HTML_LOCATION)
    # ToDo do not use this hack to do this
    if embed_style:
        with open(static_filesys.POEM_CSS_LOC) as poem_css:
            css_content = poem_css.read()
    else:
        css_content = ""
    html_poem = render.wrap_in_html_page(parsed_poem,
                                         href=css_target_loc,
                                         title=parsed_poem.title,
                                         embedded_css=css_content)

    html_file_loc = html_filesys.POEMS_LOC / (regex.sub(r"\s", "_", latex_file.stem) + ".html")
    target_file = html_root_dir / html_file_loc
    logger.info(f"Writing file {target_file}")
    poem_text = etree.tostring(html_poem, pretty_print=True)
    with open(target_file, "wb") as file:
        file.write(poem_text)
    db_interface.add_poem(parsed_poem.title, html_file_loc)

    return target_file


def parse_collection(collection: Collection,
                     html_root_dir: Path,
                     embed_style
                     ):
    html_relative_location = db_interface.get_collection_html_location(collection.name)

    # ToDo do not use this hack to do this (duplicate code from parse_latex_file)
    if embed_style:
        with open(static_filesys.COLLECTION_CSS_LOC) as poem_css:
            css_content = poem_css.read()
    else:
        css_content = ""
    html_collection = render.wrap_in_html_page(collection,
                                               "/" + str(html_filesys.COLLECTION_STYLESHEET_HTML_LOCATION),
                                               title=collection.name,
                                               embedded_css=css_content)
    target_location = html_root_dir / html_relative_location
    logger.info(f"Writing collection {target_location}")
    with open(target_location, "wb") as file:
        file.write(etree.tostring(html_collection, pretty_print=True))


def create_index(collection: Collection,
                 html_root_dir: Path,
                 embed_style: bool):
    # ToDo Clean up repeated code with parse_collection
    html_relative_location = html_filesys.INDEX_LOC
    if embed_style:
        with open(static_filesys.COLLECTION_CSS_LOC) as poem_css:
            css_content = poem_css.read()
    else:
        css_content = ""

    html_page = render.wrap_in_html_page(collection,
                                         "/" + str(html_filesys.COLLECTION_STYLESHEET_HTML_LOCATION),
                                         title=collection.name,
                                         embedded_css=css_content)
    target_location = html_root_dir / html_relative_location
    logger.info(f"Writing index file to {target_location}")
    with open(target_location, "wb") as file:
        file.write(etree.tostring(html_page, pretty_print=True))


def create_stylesheets(poem_src_stylesheet: Path, collection_src_stylesheet: Path, html_root_dir: Path):
    if not poem_src_stylesheet.is_file():
        raise ValueError(f"No css file found at expected location {poem_src_stylesheet} for poem css")
    if not collection_src_stylesheet.is_file():
        raise ValueError(f"No css file found at expected location {collection_src_stylesheet} for collection css")

    poem_css_target_loc = html_root_dir / html_filesys.POEM_STYLESHEET_HTML_LOCATION
    collection_css_target_loc = html_root_dir / html_filesys.COLLECTION_STYLESHEET_HTML_LOCATION
    logger.info(f"Copying poem css file from '{html_root_dir} to {poem_css_target_loc}")
    shutil.copy(poem_src_stylesheet, poem_css_target_loc)
    logger.info(f"Copying collection css file from '{collection_src_stylesheet} to {collection_css_target_loc}")
    shutil.copy(collection_src_stylesheet, collection_css_target_loc)
    html_filesys.setup_html_filesys(html_root_dir)


def run_workflow(latex_dir: Path,
                 html_dir: Path,
                 collection_json_loc: Path,
                 is_for_local_test=False):
    # ToDo split in sub methods for clarity
    # ToDo clearer separation between reading into database and generating HTML
    embed_style = is_for_local_test
    make_relative_links = is_for_local_test
    latex_dir = Path(latex_dir)
    html_root_dir = Path(html_dir)
    poem_css_loc = static_filesys.POEM_CSS_LOC
    collection_css_loc = static_filesys.COLLECTION_CSS_LOC
    collection_json_loc = Path(collection_json_loc)
    if not latex_dir.is_dir():
        raise ValueError(f"No directory with latex sources found at expected location {latex_dir}")
    if not html_root_dir.is_dir():
        logger.warning(f"No target directory where to create the html files was found at location {html_dir}. "
                        f"It will be created")
        html_root_dir.mkdir()
    if not collection_json_loc.is_file():
        raise ValueError(f"No file with collection info found at expected location {collection_json_loc}")

    if static_filesys.POEMS_JSON.is_file():
        logger.warning(f"Deleting existing poems database: {static_filesys.POEMS_JSON}")
        static_filesys.POEMS_JSON.unlink()

    create_stylesheets(poem_src_stylesheet=poem_css_loc,
                       collection_src_stylesheet=collection_css_loc,
                       html_root_dir=html_root_dir)
    parse_poems(latex_dir.glob("**/*.tex"),
                html_root_dir=html_root_dir,
                embed_style=embed_style
                )

    with open(collection_json_loc) as file:
        collection_info = json.load(file)

    for json_elem in [static_filesys.COLLECTION_JSON_TITLE, static_filesys.COLLECTION_JSON_CONTENT]:
        if static_filesys.COLLECTION_JSON_TITLE not in collection_info:
            raise ValueError(f"No obligatory json element '{json_elem}' found in collection json {collection_json_loc}")
    collection_title = collection_info[static_filesys.COLLECTION_JSON_TITLE]
    content = collection_info[static_filesys.COLLECTION_JSON_CONTENT]
    collection_subtitle = collection_info.get(static_filesys.COLLECTION_JSON_SUBTITLE, "")

    all_poetry = db_interface.get_all_poem_titles()
    standardised_titles = html_norm_helpers.fuzzy_find_texts(to_search_texts=content,
                                                             searchable_texts=all_poetry
                                                             )
    collection_html_loc = html_filesys.COLLECTIONS_LOC / \
                          html_norm_helpers.get_html_filename_from_normal_name(collection_title)

    if make_relative_links:
        content_title_to_loc_dict = db_interface.map_content_titles_to_location(
            standardised_titles,
            make_relative=True,
            reference_relative_dir=collection_html_loc
        )
    else:
        content_title_to_loc_dict = db_interface.map_content_titles_to_location(standardised_titles)

    collection = Collection(title=collection_title,
                            content_title_to_location_dict=content_title_to_loc_dict,
                            subtitle=collection_subtitle
                            )
    db_interface.add_collection(collection, collection_html_loc)
    parse_collection(collection, html_root_dir=html_root_dir, embed_style=embed_style)

    if make_relative_links:
        # ToDo fix (very) ugly code
        # (Requires different relative links for index page than for collection page--solved here in lazy way)
        content_title_to_loc_dict = db_interface.map_content_titles_to_location(
            standardised_titles,
            make_relative=True,
            reference_relative_dir=html_filesys.INDEX_LOC.parent
        )
        collection = Collection(title=collection_title,
                                content_title_to_location_dict=content_title_to_loc_dict,
                                subtitle=collection_subtitle
                                )

    create_index(collection, html_root_dir=html_root_dir, embed_style=embed_style)
