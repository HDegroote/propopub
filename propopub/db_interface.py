import json
import logging
import os
from pathlib import Path
from typing import Dict
import typing
from propopub import static_filesys
from propopub.collection import Collection

logger = logging.getLogger(__name__)


def _get_collections_info() -> Dict:
    collections_json = static_filesys.COLLECTIONS_JSON
    collections_info = {}
    if collections_json.is_file():
        with open(collections_json, "r") as file:
            collections_info = json.load(file)
    return collections_info


def add_poem(title: str, html_location: Path):
    poems_json = static_filesys.POEMS_JSON

    poems_info = {}
    if poems_json.is_file():
        with open(poems_json, "r") as file:
            poems_info = json.load(file)

    if title in poems_info:
        logger.warning(f"An entry for a poem with title {title} already exists in {poems_json}--overwriting the entry")
    poems_info[title] = str(html_location)

    with open(static_filesys.POEMS_JSON, "w") as file:
        json.dump(poems_info, file, indent=1)


def get_all_poem_titles() -> typing.Collection[str]:
    poems_json = static_filesys.POEMS_JSON
    poems_info = {}
    if poems_json.is_file():
        with open(poems_json, "r") as file:
            poems_info = json.load(file)
    return poems_info.keys()


def add_collection(collection: Collection, html_location: Path):
    collections_info = _get_collections_info()
    available_poem_titles = get_all_poem_titles()
    for poem_title in collection.content_titles:
        if poem_title not in available_poem_titles:
            raise ValueError(f"Attempting to create a collection with a poem title which does not exist ({poem_title})."
                             f"\nAvailable titles: {', '.join(available_poem_titles)}"
                             )

    collection_title = collection.name
    if collection_title in collections_info:
        logger.warning(f"entry for title '{collection_title}' already exists: {collections_info[collection_title]}"
                        f"--overwriting the entry")
    collections_info[collection_title] = {
        static_filesys.COLLECTION_JSON_HTML_LOCATION: str(html_location),
        static_filesys.COLLECTION_JSON_CONTENT: collection.content_titles
    }

    with open(static_filesys.COLLECTIONS_JSON, "w") as file:
        json.dump(collections_info, file, indent=1)


def get_collection_html_location(name: str) -> str:
    collections_info = _get_collections_info()
    if name not in collections_info:
        raise ValueError(f"No collection with name {name} found. Available collections: {collections_info.keys()}")
    else:
        return collections_info[name][static_filesys.COLLECTION_JSON_HTML_LOCATION]


def map_content_titles_to_location(titles: typing.Collection[str],
                                   make_relative: bool = False,
                                   reference_relative_dir: Path = None) -> Dict[str, str]:
    if make_relative and reference_relative_dir is None:
        raise ValueError("Argument reference_relative_dir is required when make_relative is True")
    if not make_relative and reference_relative_dir is not None:
        logger.warning("Argument reference_relative_dir is ignored when make_relative is False")
    
    poems_json = static_filesys.POEMS_JSON
    poems_info = {}
    if poems_json.is_file():
        with open(poems_json, "r") as file:
            poems_info = json.load(file)

    res_map = {}
    for wanted_title in titles:
        if wanted_title not in poems_info.keys():
            raise ValueError(f"No entry for content with title {wanted_title} exists")
        poem_path = Path("/") / Path(poems_info[wanted_title])
        if make_relative:
            # ToDo find cleaner way to remove "/" at start
            poem_path = str(os.path.relpath(poems_info[wanted_title], reference_relative_dir))
            if poem_path[0] == "/":
                poem_path = poem_path[1:]
        res_map[wanted_title] = str(poem_path)

    return res_map
