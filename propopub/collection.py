from typing import List, Dict
from xml.etree.ElementTree import Element
from lxml import etree
from lxml.builder import E
from propopub.render import HTMLRenderable


class Collection(HTMLRenderable):
    LIST_ITEM_STYLE_NAME = "collectionPage"
    COLLECTION_TAG = "collection"

    def __init__(self,
                 title: str,
                 content_title_to_location_dict: Dict[str, str],
                 subtitle: str = ""):
        self.title = title
        self.subtitle = subtitle
        self.content_title_to_location_dict = content_title_to_location_dict

    def has_subtitle(self):
        return len(self.subtitle) > 0

    @property
    def name(self):
        res = self.title
        if self.has_subtitle():
            res += " " + self.subtitle
        return res

    @property
    def content_titles(self) -> List[str]:
        # Source: https://stackoverflow.com/questions/16819222/how-to-return-dictionary-keys-as-a-list-in-python
        # Note: order important! Therefore cannot return as any kind of set
        return [*self.content_title_to_location_dict]

    def __repr__(self):
        res = f"Collection object with title {self.title} and subtitle{self.subtitle}"
        res += " \nContains content: " + "\n\t-".join(
            [content for content in self.content_title_to_location_dict.keys()])
        return res

    def get_list_of_contents(self) -> List[Element]:
        all_links = []
        for name, location in self.content_title_to_location_dict.items():
            content_url = etree.Element('a',
                                        attrib={'href': location}
                                        )
            content_url.text = name
            list_item = E.li(E.H3(content_url), self.CLASS(self.LIST_ITEM_STYLE_NAME))
            all_links.append(list_item)
        return all_links

    def _get_title_elems(self) -> List[Element]:
        title_elems = []
        collection_title = E.H1()
        collection_title.text = self.title
        title_elems.append(collection_title)
        if self.has_subtitle():
            collection_subtitle = E.div(self.CLASS("subtitle"))
            collection_subtitle.text = self.subtitle
            title_elems.append(collection_subtitle)
        return E.HEADER(*title_elems)

    def render_html(self) -> Element:
        title_elems = self._get_title_elems()
        all_links = self.get_list_of_contents()
        body_contents = [title_elems, E.ul(*all_links)]
        res = E.div(*body_contents, self.CLASS("collection"))
        return res
