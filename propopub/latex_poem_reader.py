import regex as regex
from TexSoup import TexSoup

from propopub.poem import Poem, Line, Verse, PoemChapter, Flourish, StartQuote


class LatexPoemReader:
    """Assumptions on tex file:
    -The tex file follows the guidelines for the verse environment
    -Note that only a small subset of verse environment functionality is parsed
    -If the poem contains multiple chapters this is expressed by defining multiple verse environments
    -If a poem chapter has a title, this is defined by a poem_title{} INSIDE the chapter's verse environment
    -A poem with chapters whose chapters have titles must define the title of the poem as a whole in a title{}
    """

    VERSE_ENVIRONMENT_NAME = "verse"
    PRE_START_LINE = r"\begin{verse}"
    POST_END_LINE = r"\end{verse}"
    HAS_TEXT_REGEX = regex.compile(r"\w+")
    INDENT_SYMBOL = r"\vin"
    VERSE_END_SYMBOL = r"\\!"
    FLOURISH_SYMBOL = r"\flourish"

    BEGIN_RIGHT_ALIGN = r"\begin{flushright}"
    END_RIGHT_ALIGN = r"\end{flushright}"
    ITALICE_ENV = r"\textit{"
    QUOTE_ENVIRONMENT_NAME = "quote"

    @classmethod
    def parse_verse_environment(cls, verse_env):
        title_env = verse_env.poemtitle
        title = ""
        if title_env is not None:
            title = title_env.string

        # Ugly code for filtering out poem title. Fails if enter inside it (which is valid latex)
        latex_lines = [line for line in str(verse_env).split("\n") if r"\poemtitle{" not in line]
        first_line, last_line = cls.get_first_and_last_line(latex_lines)

        verse_contents = []
        verses = []

        is_right_aligned = False
        is_italicised = False
        for line_nr in range(first_line, last_line):
            line = latex_lines[line_nr]

            if cls.BEGIN_RIGHT_ALIGN in line:
                if len(verse_contents) > 0:
                    pass
                    # ToDo Figure out why this error was here
                    #raise ValueError("The parser can currently not start a right aligned environment in the middle"
                    #                 "of a verse")
                is_right_aligned = True
                continue
            if cls.END_RIGHT_ALIGN in line:
                is_right_aligned = False
                continue
            if cls.ITALICE_ENV in line:
                if len(verse_contents) > 0:
                    raise ValueError("The parser can currently not start an italiced environment in the middle"
                                     "of a verse")

                is_italicised = True
                continue
            # ToDo: fix ugly code. Impossible to nest other such tags +
            # textit must be closed on a separate line, not at the end of a line with text (
            if is_italicised and "}" in line:
                if line.strip() != "}":
                    raise ValueError(f"{cls.ITALICE_ENV} was closed on a line with other text. The latex parser"
                                     f" can currently only handle italice environments which are closed on a separate"
                                     f"line.")
                is_italicised = False
                continue

            has_text = cls.HAS_TEXT_REGEX.search(line) is not None
            if has_text:
                # Note: this is a very ad-hoc solution for adding flourishes; can surely be done cleaner
                if cls.FLOURISH_SYMBOL in line:
                    verse_contents.append(Flourish())
                else:
                    # ToDo Clean up indented line logic (parsing at line level is much more complex than how it is used)
                    indent_indices, clean_line = cls.process_line(line)
                    verse_contents.append(Line(clean_line,
                                               right_aligned=is_right_aligned,
                                               italicised=is_italicised,
                                               indented=len(indent_indices) > 0))
                    if cls.VERSE_END_SYMBOL in line:
                        verse = Verse(verse_contents)
                        verses.append(verse)
                        verse_contents = []
        all_content = list(verses)
        all_content.extend(verse_contents)  # dangling lines
        return all_content, title

    @classmethod
    def parse_poem(cls, tex_poem: str):
        doc = TexSoup(tex_poem)

        start_quote = ""
        quote_environments = doc.find_all(cls.QUOTE_ENVIRONMENT_NAME)
        if len(quote_environments) > 1:
            raise NotImplementedError("No support yet for multiple quote environments")
        elif len(quote_environments) == 1:
            quoted_verses = quote_environments[0].find_all(cls.VERSE_ENVIRONMENT_NAME)
            if len(quoted_verses) == 0:
                raise NotImplementedError("Currently only support for quoted verse environments")
            if len(quoted_verses) > 1:
                raise NotImplementedError("Currently no support for multiple quoted verses (note: easy to add)")
            quoted_poem_content, _ = cls.parse_verse_environment(quoted_verses[0])
            quoted_poem = PoemChapter(quoted_poem_content)
            start_quote = StartQuote(quoted_poem)

        verse_environments = doc.find_all(cls.VERSE_ENVIRONMENT_NAME)
        verse_environments = [verse_environment for verse_environment in verse_environments
                              if verse_environment.parent.name != cls.QUOTE_ENVIRONMENT_NAME]
        if len(verse_environments) == 0:
            raise ValueError(f"ERROR: the latex poem does not contain a verse environment")

        title = cls.get_title(doc)
        if len(verse_environments) == 1:  # no multiple verse chapters
            poem_content, _ = cls.parse_verse_environment(verse_environments[0])  # Title extracted before
        else:
            chapters = []
            for verse_environment in verse_environments:
                chapter_content, chapter_title = cls.parse_verse_environment(verse_environment)
                chapter = PoemChapter(chapter_content, chapter_title)
                chapters.append(chapter)
                poem_content = chapters

        poem = Poem(title, poem_content, start_quote=start_quote)
        return poem

    @classmethod
    def process_line(cls, line):
        cleaned_line = line.strip()
        cleaned_line = cleaned_line.replace(cls.FLOURISH_SYMBOL, "")
        cleaned_line = cleaned_line.replace(cls.VERSE_END_SYMBOL, "")

        indent_indices = []
        match = cleaned_line.find(cls.INDENT_SYMBOL)
        while match >= 0:
            indent_indices.append(match)
            cleaned_line = cleaned_line[0:match] + cleaned_line[match + len(cls.INDENT_SYMBOL):]
            cleaned_line = cleaned_line.strip()
            match = cleaned_line.find(cls.INDENT_SYMBOL)

        cleaned_line = cleaned_line.replace(r"\\", "")
        cleaned_line = cleaned_line.strip()
        return indent_indices, cleaned_line

    @classmethod
    def get_first_and_last_line(cls, lines):
        first_line = 1
        try:
            while cls.PRE_START_LINE not in "".join(
                    lines[first_line - 1].split()):  # Assumes it will be found. Removes all white space chars
                first_line += 1
        except IndexError as e:
            raise ValueError(f"No potential first line was found in {lines} \nSearched for {cls.PRE_START_LINE} ({e})")
        last_line = first_line + 1
        while cls.POST_END_LINE not in "".join(lines[last_line].split()):  # Assumes it will be found
            last_line += 1
        return first_line, last_line

    @classmethod
    def get_title(cls, doc):
        """Return the title of the poem, or None if there is none"""
        if doc.title is not None:
            return str(doc.title.string)
        elif doc.poemtitle is not None:
            title = str(doc.poemtitle.string)
        else:
            title = None
        return title
