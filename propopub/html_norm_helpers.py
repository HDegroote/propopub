import logging
import re
import Levenshtein as lev
import typing
from regex import regex

logger = logging.getLogger(__name__)

def normalise_text(text: str):
    sub_regex = (r"[\s\_]", " ")
    sub_regex_remove = (r"['’]", "")

    clean_text = text
    clean_text = re.sub(sub_regex[0], sub_regex[1], clean_text)
    clean_text = re.sub(sub_regex_remove[0],
                        sub_regex_remove[1],
                        clean_text)
    clean_text = clean_text.casefold()
    return clean_text


def fuzzy_find_texts(to_search_texts: typing.Iterable[str],
                     searchable_texts: typing.Iterable[str],
                     max_lev_dist=5):
    """Finds the best match for each of the "to_search_texts" in the
    iterable of searchable_objects, based on the values in the field with
    name "object_relevant_field_name"
    The result is a list of best matches
    Throws warnings if no perfect match was found
    Throws errors if no acceptable match was found (crossover point: max_lev_dist)
    """
    res_matches = []

    for text in to_search_texts:
        max_lev_dist_title = min(max_lev_dist, len(text) // 2)
        best_match = None
        best_dist = 99999999

        to_search_title = normalise_text(text)
        for content in searchable_texts:
            norm_content_title = normalise_text(content)
            dist = lev.distance(norm_content_title, to_search_title)
            if dist < best_dist:
                best_dist = dist
                best_match = content
        if best_dist > max_lev_dist_title:
            raise ValueError(f"The best match for {text} had (after normalisation) levenshtein distance > "
                             f"{max_lev_dist_title}: {best_match} (dist {best_dist})")
        elif best_dist > 0:
            logger.warning(f"The best match for '{text}' had (after norm) non-0 levenshtein distance: "
                            f"{best_dist}: {best_match}")
        res_matches.append(best_match)
    return res_matches


def get_html_filename_from_normal_name(name: str) -> str:
    new_name = name
    new_name = regex.sub(r"[\\\s/\-]", "_", new_name)
    new_name = regex.sub(r"[\.,\?!\(\)]", "", new_name)
    new_name += ".html"
    new_name = new_name.casefold()
    return new_name
