from typing import Iterable, List
from xml.etree.ElementTree import Element

from lxml.builder import E
from lxml import etree
from propopub.render import HTMLRenderable


class Flourish(HTMLRenderable):
    CLASS_NAME = "flourish"

    def __init__(self, symbol="***"):
        self.symbol = symbol

    def __repr__(self):
        res = "Flourish object: " + self.symbol
        return res

    def render_html(self) -> etree.Element:
        res = E.H3(self.CLASS(self.CLASS_NAME))
        res.text = self.symbol
        return res


class Line(HTMLRenderable):
    CLASS_NAME = "poem_line"
    INDENTED_CLASS_NAME = "indented_poem_line"
    RIGHT_ALIGN_STYLE_VALUE = "float:right;"
    ITALICISED_STYLE_VALUE = "font-style:italic;"

    def __init__(self, content: str,
                 right_aligned=False,
                 italicised=False,
                 indented=False
                 ):
        self.content = content
        self.right_aligned = right_aligned
        self.italicised = italicised
        self.indented = indented

    def __repr__(self):
        res = "Line object: " + self.content
        if self.right_aligned:
            res += " (Right aligned)"
        if self.italicised:
            res += " (Italicised)"
        if self.indented:
            res += " (Indented)"
        return res

    def render_html(self) -> etree.Element:
        span_class = self.CLASS_NAME
        if self.indented:
            span_class = self.INDENTED_CLASS_NAME
        res = E.span(self.CLASS(span_class))

        extra_style_info = ""
        if self.right_aligned:
            extra_style_info += self.RIGHT_ALIGN_STYLE_VALUE
        if self.italicised:
            extra_style_info += self.ITALICISED_STYLE_VALUE
        if len(extra_style_info) > 0:
            res.attrib["style"] = extra_style_info
        res.text = self.content
        return res


class Verse(HTMLRenderable):
    CLASS_NAME = "verse"

    def __init__(self, lines: Iterable[Line]):
        self.lines = list(lines)

    def __repr__(self):
        res = "verse object with lines: " + repr(self.lines)
        return res

    def render_html(self) -> etree.Element:
        res = E.span(self.CLASS(self.CLASS_NAME))
        for element in self.lines:
            res.append(element.render_html())
            res.append(E.BR())
        return res


class PoemChapter(HTMLRenderable):
    CLASS_NAME = "poem_chapter"
    CHAPTER_TITLE_CLASS = "poem_chapter_title"

    def __init__(self, contents: Iterable[HTMLRenderable], title: str = ""):
        self.contents = list(contents)
        self.title = title

    def has_title(self):
        return self.title != ""

    def __repr__(self):
        res = f"PoemChapter object with title '{self.title}' and content:"
        for content in self.contents:
            res += repr(content) + "\n"
        return res

    def __render_title(self) -> etree.Element:
        title = E.H3(self.CLASS(self.CHAPTER_TITLE_CLASS))
        title.text = self.title
        return title

    def render_html(self) -> etree.Element:
        title_html = self.__render_title()
        res = E.span(self.CLASS(self.CLASS_NAME))
        res.append(title_html)
        for element in self.contents:
            res.append(element.render_html())
            res.append(E.BR())
        return res


class Poem(HTMLRenderable):
    POEM_TAG = "poem"
    TITLE_CLASS = "poem_title"
    HEADER_CLASS = "poem_header"

    def __init__(self,
                 title: str,
                 contents: Iterable[HTMLRenderable],
                 start_quote: HTMLRenderable = ""
                 ):
        self.title: str = title
        self.contents: List[HTMLRenderable] = list(contents)
        self.start_quote = start_quote

    def __repr__(self):
        res = "Poem object: \n"
        res += self.title + "\n\n"
        for content in self.contents:
            res += str(content) + "\n"
        if self.start_quote:
            res += f"Start quote: '{self.start_quote}'"
        return res

    def __render_title(self) -> etree.Element:
        title = E.H1(self.CLASS(self.TITLE_CLASS))
        title.text = self.title
        return title

    def _render_poem_header(self):
        header_div = E.div(self.CLASS(self.HEADER_CLASS))
        header_div.append(self.__render_title())
        if self.start_quote:
            header_div.append(self.start_quote.render_html())
        return header_div

    def render_html(self) -> etree.Element:
        res = E.div(self.CLASS(self.POEM_TAG))
        header_div = self._render_poem_header()
        res.append(header_div)

        for element in self.contents:
            res.append(element.render_html())
            res.append(E.BR())

        actual_res = E.div(self.CLASS("center"))
        actual_res.append(res)
        return actual_res


class StartQuote(HTMLRenderable):
    QUOTE_CLASS = "quote"
    QUOTE_ENV_CLASS = "quote_env"

    def __init__(self, content: HTMLRenderable):
        self.content: HTMLRenderable = content

    def render_html(self) -> Element:
        # ToDo Don't do so ad hoc
        res_div = E.div(self.CLASS(self.QUOTE_ENV_CLASS))
        quote_content = E.div(self.CLASS(self.QUOTE_CLASS), self.content.render_html())
        res_div.append(quote_content)
        return res_div
