import logging


def setup_logging(level=logging.INFO):
    logging.basicConfig(format='[%(levelname)s] %(asctime)s--%(name)s: %(message)s',
                        level=level)